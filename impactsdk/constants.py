IMPACT_OFFLINE_URL = 'http://testsyncticketduniyawebservice.valuablegroup.com/Service.svc?wsdl'
IMPACT_ONLINE_URL = 'http://testticketduniyawebservice.valuablegroup.com/Service.svc?wsdl'

SOAP_BODY_ONLINE = """
    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:To>http://testsyncticketduniyawebservice.valuablegroup.com/Service.svc?wsdl</wsa:To>
            <wsa:Action>http://tempuri.org/IOnlineDataService/{0}</wsa:Action>
        </soap:Header>
       <soap:Body>
          {1}
       </soap:Body>
    </soap:Envelope> """

SOAP_BODY_OFFLINE = """<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
          <wsa:To>http://testsyncticketduniyawebservice.valuablegroup.com/Service.svc</wsa:To>
          <wsa:Action>http://tempuri.org/IOfflineDataService/{0}</wsa:Action>
       </soap:Header>
       <soap:Body>
          {1}
       </soap:Body>
     </soap:Envelope>"""

OFFLINE_METHODS = {
    'GetTheaterShowDetails':"""<tem:GetTheaterShowDetails>
             <!--Optional:-->
             <tem:AccountID>{0}</tem:AccountID>
          </tem:GetTheaterShowDetails>""",
                         'GetBlankScreenLayout':"""<tem:GetBlankScreenLayout>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
      </tem:GetBlankScreenLayout>""",

            'GetExhibitorShowRatesDetails':"""
                               <tem:GetExhibitorShowRatesDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
         <!--Optional:-->
         <tem:ExhibitorAccountID>{1}</tem:ExhibitorAccountID>
         <!--Optional:-->
         <tem:Showkey_Date>{2}</tem:Showkey_Date>
      </tem:GetExhibitorShowRatesDetails>
                         """,

         'GetExhibitorTheaterShowDetails':"""
                               <tem:GetExhibitorTheaterShowDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
         <!--Optional:-->
         <tem:ExhibitorAccountID>{1}</tem:ExhibitorAccountID>
         <!--Optional:-->
         <tem:Showkey_Date>{2}</tem:Showkey_Date>
      </tem:GetExhibitorTheaterShowDetails>
                         """,

                       'GetMovieDetails': """<tem:GetMovieDetails>
         <!--Optional:-->
         <tem:MovieID>{0}</tem:MovieID>
      </tem:GetMovieDetails>""",

                         'GetShowRatesDetails':"""
                               <tem:GetShowRatesDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
      </tem:GetShowRatesDetails>
                         """,

                         'GetShowSessionDetails':"""
                               <tem:GetShowSessionDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
      </tem:GetShowSessionDetails>
                         """,

     'GetTheaterClassDetails':"""
                               <tem:GetTheaterClassDetails>
                                 <!--Optional:-->
                                 <tem:AccountID>{0}</tem:AccountID>
                              </tem:GetTheaterClassDetails>
                         """,

        'GetTheaterDetails':"""
              <tem:GetTheaterDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
      </tem:GetTheaterDetails>
        """ ,

         'GetTheaterScreenClassDetails':"""
                               <tem:GetTheaterScreenClassDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
      </tem:GetTheaterScreenClassDetails>
                         """,

         'GetTheaterScreenDetails':"""
                               <tem:GetTheaterScreenDetails>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
      </tem:GetTheaterScreenDetails>
                         """,

         'GetTheaterShowWebQuota':"""
                         <tem:GetTheaterShowWebQuota>
         <!--Optional:-->
         <tem:AccountID>{0}</tem:AccountID>
         <!--Optional:-->
         <tem:ExhibitorAccountID>{1}</tem:ExhibitorAccountID>
         <!--Optional:-->
         <tem:Showkey_Date>{2}</tem:Showkey_Date>
      </tem:GetTheaterShowWebQuota>
                         """
}

ONLINE_METHODS = {

    'Authentication':"""
                                  <tem:Authentication>
         <!--Optional:-->
         <tem:UserID>{0}</tem:UserID>
         <!--Optional:-->
         <tem:Password>{1}</tem:Password>
      </tem:Authentication>
    """,

    'GetBlockedTickets':"""

    <tem:GetBlockedTickets>
         <!--Optional:-->
         <tem:Showkey>{0}</tem:Showkey>
         <!--Optional:-->
         <tem:ClassID>{1}</tem:ClassID>
         <!--Optional:-->
         <tem:ScreenID>{2}</tem:ScreenID>
         <!--Optional:-->
         <tem:ShowTime>{3}</tem:ShowTime>
         <!--Optional:-->
         <tem:Seats>{4}</tem:Seats>
         <!--Optional:-->
         <tem:TheaterID>{5}</tem:TheaterID>
         <!--Optional:-->
         <tem:IPAddress>{6}</tem:IPAddress>
         <!--Optional:-->
         <tem:Token>{7}</tem:Token>
      </tem:GetBlockedTickets>
    """,

    'GetBookingDetails':"""
      <tem:GetBookingDetails>
         <!--Optional:-->
         <tem:Vpc_transactionno>{0}</tem:Vpc_transactionno>
         <!--Optional:-->
         <tem:Vpc_receiptno>{1}</tem:Vpc_receiptno>
         <!--Optional:-->
         <tem:Blocked_ID>{2}</tem:Blocked_ID>
         <!--Optional:-->
         <tem:TotalAmount>{3}</tem:TotalAmount>
         <!--Optional:-->
         <tem:Token>{4}</tem:Token>
      </tem:GetBookingDetails>
    """,
    'CheckSession':"""
    <tem:CheckSession>
         <!--Optional:-->
         <tem:Token>{0}</tem:Token>
      </tem:CheckSession>
    """,
    'GetReferenceID':"""
    <tem:GetReferenceID>
         <!--Optional:-->
         <tem:transactionno>{0}</tem:transactionno>
         <!--Optional:-->
         <tem:receiptno>{1}</tem:receiptno>
         <!--Optional:-->
         <tem:Token>{2}</tem:Token>
      </tem:GetReferenceID>
    """,
    'MarkRefundTransaction':"""
    <tem:MarkRefundTransaction>
         <!--Optional:-->
         <tem:transactionno>{0}</tem:transactionno>
         <!--Optional:-->
         <tem:receiptno>{1}</tem:receiptno>
         <!--Optional:-->
         <tem:Token>{2}</tem:Token>
      </tem:MarkRefundTransaction>
    """,

    'ReleaseBlockedTickets':"""
    <tem:ReleaseBlockedTickets>
         <!--Optional:-->
         <tem:Showkey>{0}</tem:Showkey>
         <!--Optional:-->
         <tem:ClassID>{1}</tem:ClassID>
         <!--Optional:-->
         <tem:ShowDate>{2}</tem:ShowDate>
         <!--Optional:-->
         <tem:Seat>{3}</tem:Seat>
         <!--Optional:-->
         <tem:TheaterID>{4}</tem:TheaterID>
      </tem:ReleaseBlockedTickets>
    """,
    'GetBlockedSeatsForLayout':"""

        <tem:GetBlockedSeatsForLayout>
         <!--Optional:-->
         <tem:Showkey>{0}</tem:Showkey>
         <!--Optional:-->
         <tem:ClassID>{1}</tem:ClassID>
         <!--Optional:-->
         <tem:ScreenID>{2}</tem:ScreenID>
         <!--Optional:-->
         <tem:ShowTime>{3}</tem:ShowTime>
         <!--Optional:-->
         <tem:TheaterID>{4}</tem:TheaterID>
         <!--Optional:-->
         <tem:Token>{5}</tem:Token>
      </tem:GetBlockedSeatsForLayout>
    """

}