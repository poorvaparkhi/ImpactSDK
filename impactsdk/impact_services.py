__author__ = 'poorvaparkhi'

from impactsdk.soap_client import impact_call
import xmltodict

class ImpactService():

    def __init__(self):
        super().__init__()

    def get_seatlayout_offline(self, *data):
        raw_data = impact_call("offline","GetBlankScreenLayout",*data)
        seat_layout = xmltodict.parse(raw_data['success'])
        seat_layout = seat_layout['s:Envelope']['s:Body']['GetBlankScreenLayoutResponse']['GetBlankScreenLayoutResult'][
        'b:BlankSeatLayout']
        return seat_layout

    def get_seatlayout_online(self, *data):
        raw_data = impact_call("online","GetBlockedSeatsForLayout",*data)
        dict_blocked_seats_data = xmltodict.parse(raw_data['success'])
        blocked_seat_list = dict_blocked_seats_data['s:Envelope']['s:Body']['GetBlockedSeatsForLayoutResponse']['GetBlockedSeatsForLayoutResult']['b:BlockedSeats']
        return blocked_seat_list

    def lock_seats(self, *data):
        raw_data = impact_call("online","GetBlockedTickets",*data)
        dict_blocktickets_data = xmltodict.parse(raw_data["success"])
        blocktickets = dict_blocktickets_data['s:Envelope']['s:Body']['GetBlockedTicketsResponse']['GetBlockedTicketsResult']
        return blocktickets

    def book_seats(self, *data):
        raw_data = impact_call("online","GetBookingDetails",*data)
        dict_blocked_seats = xmltodict.parse(raw_data['success'])
        booking_result = dict_blocked_seats['s:Envelope']['s:Body']['GetBookingDetailsResponse']['GetBookingDetailsResult']
        return booking_result

    def cancel_transaction(self, *data):
        raw_data = impact_call("online","ReleaseBlockedTickets",*data)
        release_blocked_tickets = xmltodict.parse(raw_data['success'])
        booking_result = release_blocked_tickets['s:Envelope']['s:Body']['ReleaseBlockedTicketsResponse']['ReleaseBlockedTicketsResult']
        return booking_result

    # def cancel_booking(self, *data):
    #     return impact_call("","",*data) #maybe refund transaction

    def get_movie(self, *data):
        raw_data = impact_call("offline","GetMovieDetails",*data)
        movie_details = xmltodict.parse(raw_data['success'])
        movie_details = movie_details['s:Envelope']['s:Body']['GetMovieDetailsResponse']['GetMovieDetailsResult']['b:Movie']
        return movie_details #get single movie

    def get_theatre(self,*data):
        raw_data = impact_call("offline","GetTheaterDetails",*data) #getsingletheatre
        theatre_details = xmltodict.parse(raw_data['success'])
        theatre_details = theatre_details['s:Envelope']['s:Body']['GetTheaterDetailsResponse']['GetTheaterDetailsResult'][
        'b:Theater']
        return theatre_details

    def get_theatreclass(self, *data):
        raw_data = impact_call("offline","GetTheaterClassDetails",*data)
        classes = xmltodict.parse(raw_data['success'])
        classes = classes['s:Envelope']['s:Body']['GetTheaterClassDetailsResponse']['GetTheaterClassDetailsResult'][
        'b:TheaterClass']
        return classes


    def get_theatrescreen(self, *data):
        raw_data = impact_call("offline","GetTheaterScreenDetails",*data)
        screens = xmltodict.parse(raw_data['success'])
        screens = screens['s:Envelope']['s:Body']['GetTheaterScreenDetailsResponse']['GetTheaterScreenDetailsResult'][
        'b:TheaterScreen']
        return screens


    def get_theatrescreenclass(self,*data):
        raw_data = impact_call("offline","GetTheaterScreenClassDetails",*data)
        classes = xmltodict.parse(raw_data['success'])
        classes = classes['s:Envelope']['s:Body']['GetTheaterScreenClassDetailsResponse'][
        'GetTheaterScreenClassDetailsResult']['b:TheaterScreenClass']
        return classes


    def get_theatreshows(self, *data):
        raw_data = impact_call("offline","GetExhibitorTheaterShowDetails",*data)
        theatre_shows = xmltodict.parse(raw_data['success'])
        theatre_shows = theatre_shows['s:Envelope']['s:Body']['GetExhibitorTheaterShowDetailsResponse'][
        'GetExhibitorTheaterShowDetailsResult']['b:TheaterShow']
        return theatre_shows


    def get_showprice(self, *data):
        raw_data = impact_call("offline","GetExhibitorShowRatesDetails",*data)
        show_prices = xmltodict.parse(raw_data['success'])
        show_prices = show_prices['s:Envelope']['s:Body']['GetExhibitorShowRatesDetailsResponse'][
        'GetExhibitorShowRatesDetailsResult']['b:ShowRates']
        return show_prices


    def authentication(self, *data):
        raw_data = impact_call("online","Authentication",*data)
        dict_data_authdata = xmltodict.parse(raw_data['success'])
        authentication_data  = dict_data_authdata['s:Envelope']['s:Body']['AuthenticationResponse']['AuthenticationResult']
        return authentication_data

    def check_session(self, *data):
        raw_data = impact_call("online","CheckSession",*data)
        dict_data_checksession = xmltodict.parse(raw_data['success'])
        check_session_data  = dict_data_checksession['s:Envelope']['s:Body']['CheckSessionResponse']['CheckSessionResult']
        return check_session_data

    def get_referenceID(self, *data):
        raw_data = impact_call("online","GetReferenceID",*data)
        dict_data_referenceid = xmltodict.parse(raw_data['success'])
        check_reference_data = dict_data_referenceid['s:Envelope']['s:Body']['GetReferenceIDResponse']['GetReferenceIDResult']
        return check_reference_data

    def get_refund(self, *data):
        raw_data = impact_call("online","MarkRefundTransaction",*data)
        dict_data_refundtransaction = xmltodict.parse(raw_data['success'])
        refundtransaction = ['s:Envelope']['s:Body']['MarkRefundTransactionResponse']['MarkRefundTransactionResult']
        return refundtransaction